/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, TextInput } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";

import LoginPage from "./containers/LoginPage";
import HomePage from "./containers/HomePage";

// To see network monitoring logs
// https://stackoverflow.com/questions/33997443/how-can-i-view-network-requests-for-debugging-in-react-native
GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;

export default createAppContainer(
  createStackNavigator(
    {
      Login: { screen: LoginPage },
      Home: { screen: HomePage }
    },
    { initialRouteName: "Login", headerMode: "none" }
  )
);
