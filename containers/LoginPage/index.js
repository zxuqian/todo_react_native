/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  AsyncStorage,
  Animated,
  TouchableOpacity
} from "react-native";

import LinearGradient from "react-native-linear-gradient";

type Props = {};
export default class LoginPage extends Component<Props> {
  state = {
    identifier: "",
    password: "",
    moveAnim: new Animated.Value(50),
    fadeAnim: new Animated.Value(0)
  };

  async componentDidMount() {
    const userKey = await AsyncStorage.getItem("userKey");
    if (userKey) {
      this.props.navigation.navigate("Home");
    }
    Animated.sequence([
      Animated.timing(this.state.moveAnim, {
        toValue: 0,
        duration: 1000
      }),
      Animated.timing(this.state.fadeAnim, {
        toValue: 1,
        duration: 1000
      })
    ]).start();
  }

  handleLoginButtonPressed = async () => {
    const res = await fetch("http://localhost:1337/auth/local", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        identifier: this.state.identifier,
        password: this.state.password
      })
    });

    try {
      const data = await res.json();
      if (data.jwt) {
        await AsyncStorage.setItem("userKey", data.jwt);
        this.props.navigation.navigate("Home");
      }
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    const { moveAnim, fadeAnim } = this.state;
    return (
      <LinearGradient
        colors={["#051937", "#004d7a", "#008793", "#00bf72", "#a8eb12"]}
        start={{ x: 0, y: 1 }}
        end={{ x: 1, y: 0 }}
        style={styles.linearGradient}
      >
        <View style={styles.container}>
          <Animated.Text
            style={{ ...styles.logo, transform: [{ translateY: moveAnim }] }}
          >
            Simple Todo
          </Animated.Text>
          <Animated.View style={{ ...styles.inputGroup, opacity: fadeAnim }}>
            <TextInput
              style={styles.inputField}
              onChangeText={text => this.setState({ identifier: text })}
              autoCapitalize="none"
              placeholder="用户名/邮箱"
              placeholderTextColor="#ffffff"
              value={this.state.identifier}
            />
            <TextInput
              style={styles.inputField}
              onChangeText={text => this.setState({ password: text })}
              autoCapitalize="none"
              secureTextEntry
              placeholder="密码"
              placeholderTextColor="#ffffff"
              value={this.state.password}
            />
          </Animated.View>
          <Animated.View style={{ opacity: fadeAnim }}>
            <TouchableOpacity onPress={this.handleLoginButtonPressed}>
              <View style={styles.buttonContainer}>
                <Text style={styles.loginButton}>登录</Text>
              </View>
            </TouchableOpacity>
            {/* <Button title="登录" onPress={this.onLoginButtonPressed} /> */}
          </Animated.View>
        </View>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  linearGradient: {
    flex: 1
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
    // backgroundColor: "#00C2F1"
  },

  logo: {
    fontSize: 48,
    marginBottom: 64,
    fontFamily: "HelveticaNeue",
    fontWeight: "100",
    color: "#ffffff"
  },
  inputGroup: {
    marginBottom: 20
  },
  inputField: {
    width: 230,
    borderBottomWidth: 1,
    borderBottomColor: "#ffffff",
    opacity: 0.8,
    marginBottom: 30,
    fontSize: 16,
    paddingBottom: 6,
    color: "#ffffff"
  },
  buttonContainer: {
    backgroundColor: "#00A88C",
    width: 230,
    height: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  loginButton: {
    color: "#ffffff",
    fontSize: 20,
    fontWeight: "300",
    textShadowRadius: 1,
    textShadowColor: "#007789",
    textShadowOffset: {
      width: 1,
      height: 0
    }
  }
});
