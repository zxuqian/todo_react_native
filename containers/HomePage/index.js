/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  FlatList,
  AsyncStorage
} from "react-native";

type Props = {};
export default class HomePage extends Component<Props> {
  state = {
    todos: []
  };
  async componentDidMount() {
    const userKey = await AsyncStorage.getItem("userKey");
    if (!userKey) {
      this.props.navigation.navigate("Login");
    } else {
      const res = await fetch("http://localhost:1337/todos", {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + userKey
        }
      });
      const data = await res.json();
      console.log(data);
      this.setState({ todos: data });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Todo 列表</Text>
        <FlatList
          style={styles.todoList}
          data={this.state.todos}
          renderItem={({ item }) => (
            <View>
              <Text style={styles.todo}>
                {item.completed ? "◉" : "○"} {item.title}
              </Text>
            </View>
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: "center",
    // alignItems: "center",
    paddingTop: 64,
    padding: 20,
    backgroundColor: "#008793"
  },
  title: {
    fontSize: 28,
    fontFamily: "Verdana",
    color: "#ffffff"
  },
  todoList: {
    // padding: 20
    marginTop: 20
  },
  todo: {
    fontSize: 24,
    marginBottom: 12,
    fontFamily: "Verdana",
    color: "#ffffff"
  }
});
